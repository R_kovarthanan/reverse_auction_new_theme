import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private _ths = new Subject<string>();
  th$ = this._ths.asObservable();

  constructor() { }


  sendMessage(message: string){
    this._ths.next(message);
  }
}
