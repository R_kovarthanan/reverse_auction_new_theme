import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from '../models/auth.models';

import { environment } from '../../../environments/environment';
import { LoaderService } from '../helpers/loader.service';
import { ApiResponse } from '../../model/api.response';

@Injectable({ providedIn: 'root' })
export class AuthfakeauthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    baseUrl = environment.adminAPIUrl;

    constructor(private http: HttpClient,private loader:LoaderService) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
        
    }

    // login(email: string, password: string) {
    //     return this.http.post<any>(`/users/authenticate`, { email, password })
    //         .pipe(map(user => {
    //             // login successful if there's a jwt token in the response
    //             if (user && user.token) {
    //                 // store user details and jwt token in local storage to keep user logged in between page refreshes
    //                 localStorage.setItem('currentUser', JSON.stringify(user));
    //                 this.currentUserSubject.next(user);
    //             }
    //             return user;
    //         }));
    // }

    login(userName: String, password: String) {
        return this.http.post<any>(this.baseUrl + 'login/validateUser', { userName, password })
            .pipe(map(user => {
                if (user.status == 200) {
                    localStorage.setItem('currentUser', user.result.userId);
                    this.currentUserSubject.next(user);
                }
                return user;
            }));
    }

    getBidList(): Observable<any> {
        return this.http.get<any>(`${this.baseUrl}order/getBidList`);
      }

      getVendorBidList(userId: any): Observable<any> {
        return this.http.get<any>(`${this.baseUrl}order/getVendorBidList/${userId}`);
      }
      getAuctionBidList(auctionId: any): Observable<any> {
        return this.http.get<any>(`${this.baseUrl}order/getAuctionBidList/${auctionId}`);
      }
      getAuctionWonBidList(auctionId: any): Observable<any> {
        return this.http.get<any>(`${this.baseUrl}order/getAuctionWonBidList/${auctionId}`);
      }

      startAuction(orderId: any,obj: any): Observable<any> {
        return this.http.post<ApiResponse>(`${this.baseUrl}order/startAuction/${orderId}`,obj);
      }

      getOrderList(userId: any): Observable<any> {
        return this.http.get<any>(`${this.baseUrl}order/getOrderList/${userId}`);
      }

      
      
      getAuctionList(userId: any): Observable<any> {
        return this.http.get<any>(`${this.baseUrl}order/getAuctionList/${userId}`);
      }

      getSelectedAuctionList(userId: any): Observable<any> {
        return this.http.get<any>(`${this.baseUrl}order/getSelectedAuctionList/${userId}`);
      }

      mapVendor(auctionId: any,orderId: any,obj: any): Observable<any> {
        return this.http.post<ApiResponse>(`${this.baseUrl}order/mapVendor/${auctionId}/${orderId}`,obj);
      }

      getVendorList(): Observable<any> {
        return this.http.get<any>(`${this.baseUrl}order/getVendorList`);
      }

      updateBidTime(data: any,): Observable<any> {
        return this.http.post(`${this.baseUrl}order/updateBidTime`,data);
      }

      endAuctionList(auctionId: any): Observable<any> {
        return this.http.get<any>(`${this.baseUrl}order/endAuctionList/${auctionId}`);
      }

      endBid(data:any): Observable<any> {
        
        return this.http.post<ApiResponse>(`${this.baseUrl}order/endBid`,data);
      }

      cancelBid(auctionId: any): Observable<any> {
        return this.http.post<ApiResponse>(`${this.baseUrl}order/cancelBid`,auctionId);
      }

      getBidHistory(orderId: any,vendorId: any,auctionId :any,bidMasterId: any): Observable<any> {
        return this.http.get<any>(`${this.baseUrl}order/getBidHistory/${orderId}/${vendorId}/${auctionId}/${bidMasterId}`);
      }

      BiddingAmount(data: any): Observable<any> {
        return this.http.post<ApiResponse>(`${this.baseUrl}order/biddingAmount`,data);
      }

      getVendorWonBidList(userId: any): Observable<any> {
        return this.http.get<any>(`${this.baseUrl}order/getVendorWonBidList/${userId}`);
      }

      createLhc(data: any): Observable<any> {
        return this.http.post<ApiResponse>(`${this.baseUrl}order/createLhc`,data);
      }

      getDashboardData(): Observable<any> {
        return this.http.get<any>(`${this.baseUrl}order/getDashboardData`);
      }


      endBidAndCreateLhc(data:any): Observable<any> {
        return this.http.post<ApiResponse>(`${this.baseUrl}order/endBidAndCreateLhc`,data);
      }








    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}
