import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AuthenticationService } from '../../../core/services/auth.service';
import { AuthfakeauthenticationService } from '../../../core/services/authfake.service';

import { OwlOptions } from 'ngx-owl-carousel-o';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';

import { environment } from '../../../../environments/environment';
import { ApexYAxis } from 'ng-apexcharts';
import { LoaderService } from 'src/app/core/helpers/loader.service';

@Component({
  selector: 'app-login2',
  templateUrl: './login2.component.html',
  styleUrls: ['./login2.component.scss']
})
/**
 * Login-2 component
 */
export class Login2Component implements OnInit {

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router, private authenticationService: AuthenticationService,
    private authFackservice: AuthfakeauthenticationService,public loaderservice:LoaderService) { }
  loginForm: FormGroup;
  submitted = false;
  error = '';
  errorMsg = '';
  returnUrl: string;
  userId: number;
  roleId: number;
  username:any;
  // userMenuList = [];

  // menu1 = [
  //   {
  //     id: 1,
  //     label: 'MENUITEMS.MENU.TEXT',
  //     isTitle: true
  //   },
  //   {
  //     id: 2,
  //     label: 'MENUITEMS.DASHBOARDS.TEXT',
  //     icon: 'bx-home-circle',
  //     badge: {
  //       variant: 'info',
  //       text: 'MENUITEMS.DASHBOARDS.BADGE',
  //     },
  //     subItems: [
  //       {
  //         id: 3,
  //         label: 'MENUITEMS.DASHBOARDS.LIST.DEFAULT',
  //         link: '/dashboard',
  //         parentId: 2
  //       },
  //     ]
  //   },

  //   {
  //     id: 4,
  //     label: 'MENUITEMS.ECOMMERCE.TEXT',
  //     icon: 'bx-store',
  //     subItems: [
  //       {
  //         id: 5,
  //         label: 'MENUITEMS.ECOMMERCE.LIST.PRODUCTS',
  //         link: '/ecommerce/products',
  //         parentId: 4
  //       },
  //       {
  //         id: 6,
  //         label: 'MENUITEMS.ECOMMERCE.LIST.PRODUCTDETAIL',
  //         link: '/ecommerce/product-detail/1',
  //         parentId: 4
  //       },
  //       {
  //         id: 7,
  //         label: 'MENUITEMS.ECOMMERCE.LIST.ORDERS',
  //         link: '/ecommerce/orders',
  //         parentId: 4
  //       },
  //     ]
  //   },

  // ];

  // set the currenr year
  year: number = new Date().getFullYear();

  ngOnInit(): void {
    document.body.classList.add('auth-body-bg')
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });

    // reset login status
    // this.authenticationService.logout();
    // get return url from route parameters or default to '/'
    // tslint:disable-next-line: no-string-literal
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  carouselOption: OwlOptions = {
    items: 1,
    loop: false,
    margin: 0,
    nav: false,
    dots: true,
    responsive: {
      680: {
        items: 1
      },
    }
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  /**
   * Form submit
   */
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    } else {
      if (environment.defaultauth === 'firebase') {
        this.authenticationService.login(this.f.email.value, this.f.password.value).then((res: any) => {
          this.router.navigate(['/dashboard']);
        })
          .catch(error => {
            this.error = error ? error : '';
          });
      } else {
        this.authFackservice.login(this.f.email.value, this.f.password.value)
          .pipe(first())
          .subscribe(
            data => {
              if (data.status == 200) {
                localStorage.setItem('userId', data.result.userId);
                localStorage.setItem('userName', data.result.userName);
                localStorage.setItem('roleId', data.result.roleId);
                // localStorage.setItem('userName',this.f.email.value);
                this.gotoList();
              }
              else {
                this.errorMsg = "Please enter valid user name and password";
              }
              // this.router.navigate(['/dashboard']);
            },
            error => {
              this.error = error ? error : '';
            });
      }
    }
  }

  gotoList() {
    
    this.userId = Number(localStorage.getItem("userId"));
    this.roleId = Number(localStorage.getItem("roleId"));
    if (this.roleId === 1054) {//vendor

      this.router.navigate(['orders/VendorBidList/', this.userId]);
     // this.router.navigate(['orders/dashboard']);
    }
    if (this.roleId === 1023) {//admin
      // this.userMenuList.push(this.menu1);
      // localStorage.setItem('userMenu', JSON.stringify(this.userMenuList));
      // this.router.navigate(['orders/VehicleRequirementList/']
      this.router.navigate(['orders/dashboard']);
      // this.router.navigate(['/orders/VehicleRequirementList']);
    }
  }
}
