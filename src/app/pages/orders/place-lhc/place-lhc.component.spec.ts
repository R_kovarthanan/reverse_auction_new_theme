import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaceLhcComponent } from './place-lhc.component';

describe('PlaceLhcComponent', () => {
  let component: PlaceLhcComponent;
  let fixture: ComponentFixture<PlaceLhcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaceLhcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaceLhcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
