import { Component, OnInit } from '@angular/core';
// import { ApiService } from '../../core/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
// import { breadcrumbMessage } from '../../shared/breadcrumb-message.service';

import { AuthfakeauthenticationService } from '../../../core/services/authfake.service';
import { formatDate } from '@angular/common';
import { DataService } from 'src/app/data.service';
// import { parse } from 'date-fns';

@Component({
  selector: 'app-place-lhc',
  templateUrl: './place-lhc.component.html',
  styleUrls: ['./place-lhc.component.css']
})
export class PlaceLhcComponent implements OnInit {

  constructor(private authFackservice: AuthfakeauthenticationService, 
    private router: Router, 
    private modalService: NgbModal, 
    private routes: ActivatedRoute,
    public datal:DataService) { }

  orderId: any;
  auctionId: any;
  orderNumber: any;
  vehicleType: any;
  route: any;
  qty: any;
  orderDate: any;
  startDate: any;
  endDate: any;
  amount: any;
  VendorName: any;
  VendorId: any;
  bidMasterId: any;
  lowestBidAmount: any;
  lastBidAmount: any;
  now: any;
  todaysDataTime: any;
  endDataTime: any;
  isButtonVisible = true;
  vendorUserId: any;
  userId: any;
  currentBid: any;

  keyPress(event: any) {
    // alert(event);
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  isNullorUndefinedorEmpty(str) {
    return (!str || str == '' || str == 'null' || str == '0' || str == null || str == undefined);
  }


  ngOnInit() {

    this.orderId = localStorage.getItem("orderId");
    this.orderNumber = localStorage.getItem("orderNumber");
    this.vehicleType = localStorage.getItem("vehicleType");
    this.route = localStorage.getItem("route");
    this.qty = localStorage.getItem("qty");
    this.orderDate = localStorage.getItem("orderDate");
    this.startDate = localStorage.getItem("startDate");
    this.endDate = localStorage.getItem("endDate");
    this.amount = localStorage.getItem("amount");
    this.auctionId = localStorage.getItem("auctionId");
    this.VendorName = localStorage.getItem("vendorName");
    this.VendorId = localStorage.getItem("vendorId");
    this.bidMasterId = localStorage.getItem("bidMasterId");
    this.vendorUserId = localStorage.getItem("userId");
    this.currentBid = localStorage.getItem("currentBid");


  }



  submit(advanceAmount: any) {
    if (advanceAmount === "") {
      // this.set.setOption('Please Enter Valid Advance amount', false);
      //alert("Please Enter Valid Advance amount")
      this.datal.sendMessage("error1")

    }
    else {
      console.log('amount===', advanceAmount);
      const data = {
        advanceAmount: advanceAmount,
        orderId: this.orderId,
        vendorId: this.VendorId,
        auctionId: this.auctionId,
        bidId: this.bidMasterId,
        bidAmount: this.currentBid,
        vehicleType:this.vehicleType,
        userId:this.vendorUserId,
      }
      this.authFackservice.createLhc(data).subscribe(data => {
        if (data.status === 200) {

          this.router.navigate(['/orders/createLhc/', this.vendorUserId]);
          // localStorage.setItem("message", "block")
          // this.datal.sendMessage(localStorage.getItem("message"))

          // this.set.setOption('LHC created succesfully', true);
        } else {
          // this.set.setOption('Failed', false);
        }
      }, error => { console.log(error.message); });
    }
  }
}
