import { Component, OnInit } from '@angular/core';
// import { IDropdownSettings } from 'ng-multiselect-dropdown';
// import { ApiService } from '../../core/api.service';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
// import { breadcrumbMessage } from '../../shared/breadcrumb-message.service';

import { AuthfakeauthenticationService } from '../../../core/services/authfake.service';

@Component({
  selector: 'app-vendor-mapping',
  templateUrl: './vendor-mapping.component.html',
  styleUrls: ['./vendor-mapping.component.css']
})
export class VendorMappingComponent implements OnInit {

  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};

  constructor(private authFackservice: AuthfakeauthenticationService, private router: Router, private modalService: NgbModal) { }
  orderId: any;
  auctionId: any;
  orderNumber: any;
  vehicleType: any;
  route: any;
  qty: any;
  orderDate: any;
  startDate: any;
  endDate: any;
  amount: any;

  ngOnInit() {
    this.orderId = localStorage.getItem("orderId");
    this.orderNumber = localStorage.getItem("orderNumber");
    this.vehicleType = localStorage.getItem("vehicleType");
    this.route = localStorage.getItem("route");
    this.qty = localStorage.getItem("qty");
    this.orderDate = localStorage.getItem("orderDate");
    this.startDate = localStorage.getItem("startDate");
    this.endDate = localStorage.getItem("endDate");
    this.amount = localStorage.getItem("amount");
    this.auctionId = localStorage.getItem("auctionId");

    this.getVendorList();
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'vendorId',
      textField: 'vendorName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
  }
  getVendorList() {
    this.authFackservice.getVendorList().subscribe(data => {
      if (data.status === 200) {
        this.dropdownList = data.result;
        this.selectedItems = data.result;
      }
    }, error => { console.log(error.message); });
  }

  onItemSelect(item: any) {
    console.log(this.selectedItems);
  }

  OnItemDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedItems);
    
  }

  onSelectAll(items: any) {
    console.log(items);
  }

  onDeSelectAll(items: any) {
    console.log(items);
  }

  submit() {
    // console.log(this.selectedItems);
    // console.log(this.selectedItems.length);


    if(this.selectedItems.length > 0){
      const data = {
        vendorList: this.selectedItems,
      }
      this.authFackservice.mapVendor(this.auctionId,this.orderId,data).subscribe(data => {
        if (data.status === 200) {
          // this.set.setOption('Mapped succesfully', true);
          this.router.navigate(['/orders/ViewAuctionList']);
        }
      }, error => { console.log(error.message); });
    }
    if(this.selectedItems.length === 0){
      // this.set.setOption('Please Map Vendor', false);
    }
    
    
  }
}

