import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorMappingComponent } from './vendor-mapping.component';

describe('VendorMappingComponent', () => {
  let component: VendorMappingComponent;
  let fixture: ComponentFixture<VendorMappingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorMappingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorMappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
