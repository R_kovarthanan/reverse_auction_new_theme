import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorBidListComponent } from './vendor-bid-list.component';

describe('VendorBidListComponent', () => {
  let component: VendorBidListComponent;
  let fixture: ComponentFixture<VendorBidListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendorBidListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorBidListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
