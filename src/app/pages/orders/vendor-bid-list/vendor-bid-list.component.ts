import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
// import { ApiService } from '../../core/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
// import { breadcrumbMessage } from '../../shared/breadcrumb-message.service';

import { AuthfakeauthenticationService } from '../../../core/services/authfake.service';

import { DatePipe, DecimalPipe } from '@angular/common';
import { AdvancedService } from '../advanced.service';
import { AdvancedSortableDirective, SortEvent } from '../advanced-sortable.directive';
import { interval, Observable } from 'rxjs';
// import { Table } from '../advanced.model';

import Swal from 'sweetalert2';
import { DataService } from 'src/app/data.service';

@Component({
  selector: 'app-vendor-bid-list',
  templateUrl: './vendor-bid-list.component.html',
  styleUrls: ['./vendor-bid-list.component.css'],
  providers: [AdvancedService, DecimalPipe]
})
export class VendorBidListComponent implements OnInit {
  end : any;
  userId: any;
  bidList: any;
  bidamount:any

  oli:any;
  searchProgramList;
  p: any;
  auctionId: any;
  vendorUserId: any;

  @ViewChildren(AdvancedSortableDirective) headers: QueryList<AdvancedSortableDirective>;
  public isCollapsed = true;

  constructor(private authFackservice: AuthfakeauthenticationService,
    private router: Router, private modalService: NgbModal, private route: ActivatedRoute,
    public service: AdvancedService,
    public datal:DataService,public datepipe:DatePipe) { }

  keyPress(event: any) {
    // alert(event);
    const pattern = /[0-9\+\-\.\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  


  ngOnInit() {
   
    this.vendorUserId = this.route.snapshot.params['userId'];

    if (this.vendorUserId) {
      this.getVendorBidList();
    }

  }

  onSort({ column, direction }: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });
    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }

  //intervel(value){
  // console.log(value)
    // var  datenow = new Date(value).getTime()
    // var x = setInterval(function() {
    //   var lety= new Date().getTime()
    //   var dis=datenow -lety;
    //   var hours = Math.floor((dis % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    //   var minutes = Math.floor((dis % (1000 * 60 * 60)) / (1000 * 60));
    //   var seconds = Math.floor((dis % (1000 * 60)) / 1000);
    //   this.end =  hours +":"+minutes+":"+seconds;
    //   return "ghjgjhjg"
  // },1000)

  //}
  

  getVendorBidList() {
    this.authFackservice.getVendorBidList(this.vendorUserId).subscribe(data => {
      if (data.status === 200) {
        this.bidList = data.result;
        if(data.result.rank=='L 1'){
          
        }
        for(var er = 0; er< this.bidList.length; er++){
          console.log("ttttttt"+this.bidList[er].bid_end_date)
          var fa = this.bidList[er].bid_end_date
          var day = fa[0]+fa[1]
          var month = fa[3]+fa[4]
          var year = fa[6]+fa[7]+fa[8]+fa[9]
          var hours = fa[11]+fa[12]
          var mins = fa[14]+fa[15]
          var secs = fa[17]+fa[18]
          var date = `${month} ${day} ${year} ${hours}:${mins}:${secs}`
          this.bidList[er].bid_end_date = date
          //console.log(this.bidList[er].bid_end_date )
        }

 
        // for(var g=0;g<8;g++){
        //   var  datenow = new Date(this.bidList[g].bid_end_date).getTime();
        //  var l= document.getElementsByClassName("fff")[0].innerHTML = "rtr";
        //   console.log(datenow,l)

        // }
    

         
     
        // var  datenow = new Date(this.bidList[2].bid_end_date).getTime()
        // setInterval(function() {
        //     var lety= new Date().getTime()
        //   var dis=datenow -lety;
        //   var hours = Math.floor((dis % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        // var minutes = Math.floor((dis % (1000 * 60 * 60)) / (1000 * 60));
        // var seconds = Math.floor((dis % (1000 * 60)) / 1000);
        // this.end =  hours +":"+minutes+":"+seconds;
        // document.getElementsByClassName("fff")[2].innerHTML = this.end
         
        // },1000)
       
      
   
      
        const obs$ = interval(1000);
        obs$.subscribe(() => {
          console.log();
          
        for(var g=0;g<this.bidList.length;g++){
          
          var  datenow = new Date(this.bidList[g].bid_end_date).getTime()
         //console.log(datenow+"ffffff")
          // console.log("dare==="+this.bidList[g].bid_end_time);
          
          var lety= new Date().getTime()
          var dis=datenow -lety;
          var days = Math.floor(dis / (1000 * 60 * 60 * 24));
          var hours = Math.floor((dis % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((dis % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((dis % (1000 * 60)) / 1000);
        if(days<=0&&hours<=0){
          document.getElementsByClassName("fff")[g].innerHTML = "-"
        }
        else{
          this.end = days+"d:"+ hours +"h:"+minutes+"m:"+seconds+"s";
        document.getElementsByClassName("fff")[g].innerHTML = this.end
        //console.log(this.end+"vvvvvvvvv")
        }
        
        

        }

         
          
        })
         









      //  var fa = this.bidList[2].bid_end_date
      //  var day = fa[0]+fa[1]
      //  var month = fa[3]+fa[4]
      //  var year = fa[6]+fa[7]+fa[8]+fa[9]
      //  var date = `${month} ${day} ${year}`
      //  //console.log(day,"",month)
       //console.log(date)
       
       
      //  var  datenow = new Date(date).getTime()
      //  var x = setInterval(function() {

      //  var lety= new Date().getTime()
      // let dis=datenow -lety;
      // var hours = Math.floor((dis % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      // var minutes = Math.floor((dis % (1000 * 60 * 60)) / (1000 * 60));
      // var seconds = Math.floor((dis % (1000 * 60)) / 1000);


      // this.oli=hours +":"+minutes+":"+seconds;
      // var ggg="hhjh"
      // console.log(hours +":"+minutes+":"+seconds)
      // document.getElementById("iuy").innerHTML = this.oli
       
      // console.log(lety)

      //  for(var s = 0; s < fa.length; s++){
      //   document.getElementsByClassName("ox")[s].innerHTML = this.oli
      // }
       
      //  console.log(datenow)
       //},1000);
        // for(var i=0;i<data.result.length;i++){
        //   console.log(data.result[i].bid_end_date)
        // }
        
        
      }
    }, error => { console.log(error.message); });
  }

  isNullorUndefinedorEmpty(str) {
    return (!str || str == '' || str == 'null' || str == '0' || str == null || str == undefined);
  }
  redirect(o:any) {
    localStorage.setItem('auctionId', o.auctionId);
    localStorage.setItem('orderId', o.orderId);
    localStorage.setItem('orderNumber', o.orderNumber);
    localStorage.setItem('vehicleType', o.vehicleType);
    localStorage.setItem('route', o.route);
    localStorage.setItem('qty', o.totalPackage);
    localStorage.setItem('orderDate', o.orderDate);
    localStorage.setItem('startDate', o.bid_start_date);
    localStorage.setItem('endDate', o.bid_end_date);
    localStorage.setItem('amount', o.bidAmount);
    localStorage.setItem('vendorName', o.vendorName);
    localStorage.setItem('vendorId', o.vendorId);
    localStorage.setItem('bidMasterId', o.bidMasterId);
    this.router.navigate(['/orders/PlaceBid/', this.vendorUserId]);
}


  View(o: any) {
    // localStorage.setItem('auctionId', o.auctionId);
    // localStorage.setItem('orderId', o.orderId);
    // localStorage.setItem('orderNumber', o.orderNumber);
    // localStorage.setItem('vehicleType', o.vehicleType);
    // localStorage.setItem('route', o.route);
    // localStorage.setItem('qty', o.totalPackage);
    // localStorage.setItem('orderDate', o.orderDate);
    // localStorage.setItem('startDate', o.bid_start_date);
    // localStorage.setItem('endDate', o.bid_end_date);
    // localStorage.setItem('amount', o.bidAmount);
    // localStorage.setItem('vendorName', o.vendorName);
    // localStorage.setItem('vendorId', o.vendorId);
    // localStorage.setItem('bidMasterId', o.bidMasterId);
    // this.router.navigate(['/orders/PlaceBid/', this.vendorUserId]);


    if (this.isNullorUndefinedorEmpty(o.currentBid)) {
      //alert("Please Enter Valid Bid Amount")
      //this.datal.sendMessage("error")
    }
    else{
      const data ={
        bidAmount : o.currentBid,
        orderId:o.orderId,
        vendorId:o.vendorId,
        auctionId:o.auctionId,
        bidId:o.bidMasterId
      } 

      this.authFackservice.BiddingAmount(data).subscribe(data => {
        if (data.status === 200) {
          // if(this.vendorUserId){
            // this.userId = this.vendorUserId 
            // this.router.navigate(['/orders/VendorBidList/', this.userId]);
           


            window.location.reload();
            Swal.fire({
              position: 'top-end',
              icon: 'success',
              title: 'Your Bid has been saved successfully',
              showConfirmButton: false,
              timer: 1500
            });
          // }else{
            // this.router.navigate(['/orders/VendorBidList']);  
          // }
          // this.router.navigate(['/orders/VendorBidList']);
          // this.router.navigate(['/orders/VendorBidList/', this.userId]);
          // this.set.setOption('succesfully Bidded', true);
        }else{
          // this.set.setOption('Failed', false);
        }
      }, error => { console.log(error.message); });
    }

  }

  function(o:any){
    
    if(parseFloat(o.currentBid) > parseFloat(o.bidAmount)){
      alert("please enter the valid bid amount")
      // this.datal.sendMessage("error")
    }
    if(parseFloat(o.currentBid) > parseFloat(o.lowestBid)){
      alert("please enter the valid bid amount")
    }

    
   

  }
  
  

}
