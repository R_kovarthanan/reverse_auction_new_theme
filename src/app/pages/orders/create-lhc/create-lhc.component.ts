import { Component, OnInit } from '@angular/core';
// import { ApiService } from '../../core/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
// import { breadcrumbMessage } from '../../shared/breadcrumb-message.service';

import { AuthfakeauthenticationService } from '../../../core/services/authfake.service';

@Component({
  selector: 'app-create-lhc',
  templateUrl: './create-lhc.component.html',
  styleUrls: ['./create-lhc.component.css']
})
export class CreateLhcComponent implements OnInit {

  constructor(private authFackservice: AuthfakeauthenticationService, private router: Router,
     private modalService: NgbModal,
      private route: ActivatedRoute) { }

  userId: any;
  bidList: any;
  searchProgramList;
  p: any;
  auctionId: any;
  vendorUserId: any;
  

  ngOnInit() {
    this.vendorUserId = this.route.snapshot.params['userId'];
    if (this.vendorUserId) {
    this.getVendorWonBidList();
    }

  }

  getVendorWonBidList() {
    this.authFackservice.getVendorWonBidList(this.vendorUserId).subscribe(data => {
      if (data.status === 200) {
        console.log(data.result);
        this.bidList = data.result;
        
      }
    }, error => { console.log(error.message); });
  }



  View(o: any) {
    localStorage.setItem('auctionId', o.auctionId);
    localStorage.setItem('orderId', o.orderId);
    localStorage.setItem('orderNumber', o.orderNumber);
    localStorage.setItem('vehicleType', o.vehicleType);
    localStorage.setItem('route', o.route);
    localStorage.setItem('qty', o.totalPackage);
    localStorage.setItem('orderDate', o.orderDate);
    localStorage.setItem('startDate', o.bid_start_date);
    localStorage.setItem('endDate', o.bid_end_date);
    localStorage.setItem('amount', o.bidAmount);
    localStorage.setItem('vendorName', o.vendorName);
    localStorage.setItem('vendorId', o.vendorId);
    localStorage.setItem('bidMasterId', o.bidMasterId);
    localStorage.setItem('currentBid', o.currentBid);
    localStorage.setItem('userId', this.vendorUserId);
    this.router.navigate(['/orders/placeLhc']);
    
  }

}
