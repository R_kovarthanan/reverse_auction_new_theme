import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateLhcComponent } from './create-lhc.component';

describe('CreateLhcComponent', () => {
  let component: CreateLhcComponent;
  let fixture: ComponentFixture<CreateLhcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateLhcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateLhcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
