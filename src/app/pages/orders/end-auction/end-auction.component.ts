import { Component, OnInit } from '@angular/core';
// import { ApiService } from '../../core/api.service';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
// import { breadcrumbMessage } from '../../shared/breadcrumb-message.service';
// import { DatePipe } from '@angular/common';
import * as moment from 'moment/moment.js';
import { AuthfakeauthenticationService } from '../../../core/services/authfake.service';

@Component({
  selector: 'app-end-auction',
  templateUrl: './end-auction.component.html',
  styleUrls: ['./end-auction.component.css']
})
export class EndAuctionComponent implements OnInit {

  constructor(private authFackservice: AuthfakeauthenticationService,
     private router: Router, private modalService: NgbModal) { }
orderId:any;
route:any;
advance:any;
auctionId:any;
userId:any;
auctionList: any;
orderDate:any;
orderNumber:any;
vehicleType:any;
bid_end_date_time:any;
bid_start_date:any;
bid_end_date:any;
bid_end_time:any;
date_time:any;
bid_end_hr_min:any;
bid_end_hr:any;
bid_end_min:any;
bid_end_sec:any;
end_time:any;
date:any;
endBidButton:boolean=false;
remarks:any;
update(){
  const data = {
    orderId:this.orderId,
    bidEndDate:this.date,
    bidEndTime:this.bid_end_time,
  }
  this.authFackservice.updateBidTime(data).subscribe(data => {
    if (data.status === 200) {
      // console.log(data.result);
      // this.auctionList = data.result;
      this.router.navigate(['/orders/ViewAuctionList']);
    }
  }, error => { console.log(error.message); });
}
isNullorUndefinedorEmpty(str) {
  return (!str || str == '' || str == 'null' || str == '0' || str == null || str == undefined);
}
  ngOnInit() {
     this.orderId = localStorage.getItem("orderId");
    this.route = localStorage.getItem("route");
    this.userId=  localStorage.getItem("userId");
    this.auctionId = localStorage.getItem("auctionId");
    this.orderDate = localStorage.getItem("orderDate");
    this.orderNumber = localStorage.getItem("orderNumber");
    this.vehicleType=localStorage.getItem("vehicleType");
    this.bid_end_date_time = localStorage.getItem("bidEndDate");

    this.bid_start_date = localStorage.getItem("bidStartDate");
    this.date_time = this.bid_end_date_time.split(" ");
    this.bid_end_date = this.date_time[0];
    var newdate = new Date(this.bid_end_date.substring(6, 10), Number(this.bid_end_date.substring(3, 5)) - 1, this.bid_end_date.substring(0, 2));
    this.date=moment(newdate).format("YYYY-MM-DD");
    this.bid_end_time = this.date_time[1];
    this.endAuctionList();
  }

  endAuctionList() {
    this.authFackservice.endAuctionList(this.auctionId).subscribe(data => {
      if (data.status === 200) {
        console.log(data.result);
        this.auctionList = data.result;
      }
    }, error => { console.log(error.message); });
  }
  
  validateCheckBox(index,o){
    var checkbox = document.getElementsByName("check");
    if(!this.isNullorUndefinedorEmpty(o.remarks)&&!this.isNullorUndefinedorEmpty(o.advanceAmount)){
      this.advance=o.advanceAmount;
      console.log("if condition");
      for (let i = 0; i < checkbox.length; i++) {
        (checkbox[i] as HTMLInputElement).checked=false;
      }
      (checkbox[index] as HTMLInputElement).checked=true;
      this.endBidButton=true
    }else{
      // for (let i = 0; i < checkbox.length; i++) {
        (checkbox[index] as HTMLInputElement).checked=false;
        this.endBidButton=false
      // }
      // alert("please enter advance amount & remarks");
      console.log("please enter advance amount & remarks");
    }
    
    
     if(parseFloat(o.advanceAmount) > parseFloat(o.lowestBid)){
      (checkbox[index] as HTMLInputElement).checked=false;
       this.endBidButton=false;
        alert("please enter the valid advance amount")
        // this.datal.sendMessage("error")
      }
    
    // for (let i = 0; i < checkbox.length; i++) {
    //   (checkbox[i] as HTMLInputElement).checked=false;
    // }
    // (checkbox[index] as HTMLInputElement).checked=true;
  }
checkBoxValidation(o:any,advanceAmount:any,i:any){
  this.auctionList[i].checks=false;
  // console.log("check1==="+o.checks);
  
  // console.log("index==="+i);
  // console.log("remarks==="+this.isNullorUndefinedorEmpty(o.remarks));
  // console.log("advance==="+advanceAmount);

  
  
  // if(o.checks  == true){
  //   console.log("tick2");
  //   o.checks=false;
  //   console.log("o.checks3=="+o.checks);
  // }else{
  //   console.log("untick");
  // }
  


  
  // if(this.isNullorUndefinedorEmpty(advanceAmount) || this.isNullorUndefinedorEmpty(o.remarks)){
    // alert("Please Enter Advance Amount and Remarks for selection")
    // for(let i=0;i<this.auctionList.length;i++){
      // if(this.auctionList[i].check==true){
        // this.auctionList[i].check=false;
        // alert("i="+i + o.check +this.auctionList[i].check)
        
      // }
    // }
    // this.auctionList[1].check=false;
    // console.log("sasa=="+o.check);
  // }
// else if(this.isNullorUndefinedorEmpty(advanceAmount)){
//   alert("Please Enter Advance Amount for selection")
//   for(let i=0;i<this.auctionList.length;i++){
//     if(this.auctionList[i].check==true){
//       this.auctionList[i].check=false;
//     }
//   }
//   o.check=false;
//   console.log("sasa=="+o.check);
// } // alert("remarks==="+o.remarks);
//   else if (this.isNullorUndefinedorEmpty(o.remarks)) {
//     // console.log(this.isNullorUndefinedorEmpty(o.remarks));
//     // this.set.setOption('Please Enter Remarks for selection', false);
//     alert("Please Enter Remarks for selection")
//     for(let i=0;i<this.auctionList.length;i++){
//       if(this.auctionList[i].check==true){
//         this.auctionList[i].check=false;
//         console.log("sasa=="+o.check)
//       }
//     }
  //   o.check=false;
  //   console.log("sasa=="+o.check);
  //   // console.log("this.auctionList ::"+o.check);
    
  // }
  // else{
    // this.endBidButton=o.check;
    // console.log("this.auctionList ::"+o.check);
    

    // for(let i=0;i<this.auctionList.length;i++){
      // if(this.auctionList[i].check==true){
        // this.auctionList[i].check=false;
        // console.log("sasa=="+o.check)
      // }
    // }
    // o.check=true;
    // console.log(advanceAmount);
    
  // }
}
endBid(){
  // var count=0;
  for(let i=0;i<this.auctionList.length;i++){
    // console.log("i=="+i)
    // console.log("checkbox==="+this.auctionList[i].checks)
    
    if(this.auctionList[i].checks==true){
      // this.auctionList[i].
      // console.log(this.auctionList[i]);
      const data = {
        auctionId:this.auctionId,
        bidMasterId:this.auctionList[i].bidMasterId,
        vendorId:this.auctionList[i].vendorId,
        remarks:this.auctionList[i].remarks,
        orderId:this.orderId,
        bidAmount:this.auctionList[i].lowestBid,
        advanceAmount:this.advance,
        userId:this.userId,
  
      }
      console.log("data inside==="+data)
      
      
      this.authFackservice.endBidAndCreateLhc(data).subscribe(data => {
        
        if (data.status === 200) {
          console.log(data.result);
          // this.auctionList = data.result;
          this.router.navigate(['/orders/ViewAuctionList']);
        }
      }, error => { console.log(error.message); });



    }
  }
}
  showDialog(){
    if(window.confirm('Are sure you want to cancel this Auction ?')){
      this.authFackservice.cancelBid(this.auctionId).subscribe(data => {
        if (data.status === 200) {
          // console.log(data.result);
          // this.auctionList = data.result;
          this.router.navigate(['/orders/ViewAuctionList']);
        }
      }, error => { console.log(error.message); });
      
     }
  }

}
