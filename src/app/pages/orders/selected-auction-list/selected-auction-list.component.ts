import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthfakeauthenticationService } from 'src/app/core/services/authfake.service';

@Component({
  selector: 'app-selected-auction-list',
  templateUrl: './selected-auction-list.component.html',
  styleUrls: ['./selected-auction-list.component.scss']
})
export class SelectedAuctionListComponent implements OnInit {

  constructor(private authFackservice: AuthfakeauthenticationService, private router: Router, private modalService: NgbModal) { }




  userId: any;
  auctionList: any;

  x = 0;


  map(o: any) {
    localStorage.setItem('orderId',o.orderId);
    localStorage.setItem('orderNumber',o.orderNumber);
    localStorage.setItem('vehicleType',o.vehicleType);
    localStorage.setItem('route',o.route);
    localStorage.setItem('qty',o.totalPackage);
    localStorage.setItem('orderDate',o.orderDate);
    localStorage.setItem('startDate',o.bid_start_date);
    localStorage.setItem('endDate',o.bid_end_date);
    localStorage.setItem('amount',o.bidAmount);
    localStorage.setItem('auctionId',o.auctionId);
    
    this.router.navigate(['/orders/VendorMapping']);
  }

  ngOnInit() {
    this.userId = localStorage.getItem("userId");
    this.getAuctionList();
  }

  getAuctionList() {
    this.authFackservice.getSelectedAuctionList(this.userId).subscribe(data => {
      if (data.status === 200) {
        console.log(data.result);
        this.auctionList = data.result;
      }
    }, error => { console.log(error.message); });
  }

  ViewBidList(o:any){
    this.router.navigate(['/orders/ViewBidList/auctionId',o.auctionId]);
  }
  ViewWonBidList(o:any){
    this.router.navigate(['/orders/ViewWonBidList/auctionId',o.auctionId]);
  }

  end(o:any){
    localStorage.setItem('orderId',o.orderId);
    localStorage.setItem('route',o.route);
    localStorage.setItem('auctionId',o.auctionId);
    localStorage.setItem('orderDate',o.orderDate);
    localStorage.setItem('orderNumber',o.orderNumber);
    localStorage.setItem('bidEndDate',o.bid_end_date);
    localStorage.setItem('bidStartDate',o.bid_start_date);
    this.router.navigate(['/orders/endAuction']);
  }

}
