import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectedAuctionListComponent } from './selected-auction-list.component';

describe('SelectedAuctionListComponent', () => {
  let component: SelectedAuctionListComponent;
  let fixture: ComponentFixture<SelectedAuctionListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectedAuctionListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectedAuctionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
