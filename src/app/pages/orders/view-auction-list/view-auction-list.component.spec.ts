import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAuctionListComponent } from './view-auction-list.component';

describe('ViewAuctionListComponent', () => {
  let component: ViewAuctionListComponent;
  let fixture: ComponentFixture<ViewAuctionListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAuctionListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAuctionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
