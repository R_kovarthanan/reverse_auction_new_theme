import { Component, OnInit } from '@angular/core';
// import { ApiService } from '../../core/api.service';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
// import { breadcrumbMessage } from '../../shared/breadcrumb-message.service';

import { AuthfakeauthenticationService } from '../../../core/services/authfake.service';

@Component({
  selector: 'app-view-auction-list',
  templateUrl: './view-auction-list.component.html',
  styleUrls: ['./view-auction-list.component.css']
})
export class ViewAuctionListComponent implements OnInit {

  constructor(private authFackservice: AuthfakeauthenticationService, private router: Router, private modalService: NgbModal) { }




  userId: any;
  auctionList: any;


  map(o: any) {
    localStorage.setItem('orderId',o.orderId);
    localStorage.setItem('orderNumber',o.orderNumber);
    localStorage.setItem('vehicleType',o.vehicleType);
    localStorage.setItem('route',o.route);
    localStorage.setItem('qty',o.totalPackage);
    localStorage.setItem('orderDate',o.orderDate);
    localStorage.setItem('startDate',o.bid_start_date);
    localStorage.setItem('endDate',o.bid_end_date);
    localStorage.setItem('amount',o.bidAmount);
    localStorage.setItem('auctionId',o.auctionId);
    
    this.router.navigate(['/orders/VendorMapping']);
  }
   

  ngOnInit() {
    this.userId = localStorage.getItem("userId");
    this.getAuctionList();
  }

  getAuctionList() {
    this.authFackservice.getAuctionList(this.userId).subscribe(data => {
      if (data.status === 200) {
        console.log(data.result);
        this.auctionList = data.result;
      }
    }, error => { console.log(error.message); });
  }

  ViewBidList(o:any){
    this.router.navigate(['/orders/ViewBidList/auctionId',o.auctionId]);
  }
  ViewWonBidList(o:any){
    this.router.navigate(['/orders/ViewWonBidList/auctionId',o.auctionId]);
  }

  end(o:any){
    localStorage.setItem('orderId',o.orderId);
    localStorage.setItem('route',o.route);
    localStorage.setItem('auctionId',o.auctionId);
    localStorage.setItem('orderDate',o.orderDate);
    localStorage.setItem('orderNumber',o.orderNumber);
    localStorage.setItem('bidEndDate',o.bid_end_date);
    localStorage.setItem('bidStartDate',o.bid_start_date);
    this.router.navigate(['/orders/endAuction']);
  }

}
