import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { AuthfakeauthenticationService } from '../../../core/services/authfake.service';

@Component({
  selector: 'app-view-won-bid-list',
  templateUrl: './view-won-bid-list.component.html',
  styleUrls: ['./view-won-bid-list.component.scss']
})
export class ViewWonBidListComponent implements OnInit {

  constructor(private authFackservice: AuthfakeauthenticationService, private router: Router, private modalService: NgbModal,
    private route: ActivatedRoute) { }

    userId: any;
  bidList: any;
  searchProgramList;
  p: any;
  auctionId: any;
  vendorUserId: any;

  ngOnInit(): void {
    this.vendorUserId = this.route.snapshot.params['userId'];
    this.auctionId = this.route.snapshot.params['auctionId'];
    if (this.vendorUserId) {
      this.getVendorBidList();
    } else if(this.auctionId) {
      this.getAuctionBidList();
    }else{
      this.getBidList();
    }
  }

  getBidList() {
    this.authFackservice.getBidList().subscribe(data => {
      if (data.status === 200) {
        console.log(data.result);
        this.bidList = data.result;
      }
    }, error => { console.log(error.message); });
  }
  getVendorBidList() {
    this.authFackservice.getVendorBidList(this.vendorUserId).subscribe(data => {
      if (data.status === 200) {
        console.log(data.result);
        this.bidList = data.result;
      }
    }, error => { console.log(error.message); });
  }
  getAuctionBidList() {
    this.authFackservice.getAuctionWonBidList(this.auctionId).subscribe(data => {
      if (data.status === 200) {
        console.log(data.result);
        this.bidList = data.result;
      }
    }, error => { console.log(error.message); });
  }

}
