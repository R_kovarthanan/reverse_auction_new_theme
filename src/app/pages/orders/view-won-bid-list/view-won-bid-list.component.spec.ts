import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewWonBidListComponent } from './view-won-bid-list.component';

describe('ViewWonBidListComponent', () => {
  let component: ViewWonBidListComponent;
  let fixture: ComponentFixture<ViewWonBidListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewWonBidListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewWonBidListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
