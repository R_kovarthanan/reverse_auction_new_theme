import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewBidListComponent } from './view-bid-list.component';

describe('ViewBidListComponent', () => {
  let component: ViewBidListComponent;
  let fixture: ComponentFixture<ViewBidListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewBidListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewBidListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
