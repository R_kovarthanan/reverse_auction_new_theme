import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrdersRoutingModule } from './orders-routing.module';
import { VehicleRequirementListComponent } from './vehicle-requirement-list/vehicle-requirement-list.component';
import { FormsModule, ReactiveFormsModule, FormGroup, Validators } from '@angular/forms';
import { VendorMappingComponent } from './vendor-mapping/vendor-mapping.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ViewAuctionListComponent } from './view-auction-list/view-auction-list.component';
import { ViewBidListComponent } from './view-bid-list/view-bid-list.component';
// import { ViewWonBidListComponent } from './view-won-bid-list/view-won-bid-list.component';

import { NgbDate, NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import {NgxPaginationModule} from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { PlaceBidComponent } from './place-bid/place-bid.component';
import { EndAuctionComponent } from './end-auction/end-auction.component';
import { VendorBidListComponent } from './vendor-bid-list/vendor-bid-list.component';
import { CreateLhcComponent } from './create-lhc/create-lhc.component';
import { PlaceLhcComponent } from './place-lhc/place-lhc.component';
//import { UIModule } from '../../shared/ui/ui.module';
import { ViewWonBidListComponent } from './view-won-bid-list/view-won-bid-list.component';
import { SelectedAuctionListComponent } from './selected-auction-list/selected-auction-list.component';

@NgModule({
  declarations: [VehicleRequirementListComponent,
     VendorMappingComponent,
     ViewAuctionListComponent,
      ViewBidListComponent,
      // ViewWonBidListComponent,
       PlaceBidComponent, 
       EndAuctionComponent,
        VendorBidListComponent,
         CreateLhcComponent,
          PlaceLhcComponent,
          ViewWonBidListComponent,
          SelectedAuctionListComponent
        ],
  imports: [
    CommonModule,
    OrdersRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgMultiSelectDropDownModule.forRoot(),
    NgbModule,
    // NgxPaginationModule,
    Ng2SearchPipeModule,
   // UIModule
  ]
})
export class OrdersModule { }
