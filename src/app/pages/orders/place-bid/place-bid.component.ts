import { Component, OnInit } from '@angular/core';
// import { ApiService } from '../../core/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
// import { breadcrumbMessage } from '../../shared/breadcrumb-message.service';
import {formatDate } from '@angular/common';
// import { parse } from 'date-fns';

import { AuthfakeauthenticationService } from '../../../core/services/authfake.service';

@Component({
  selector: 'app-place-bid',
  templateUrl: './place-bid.component.html',
  styleUrls: ['./place-bid.component.css']
})
export class PlaceBidComponent implements OnInit {

  constructor(private authFackservice: AuthfakeauthenticationService, private router: Router,
     private modalService: NgbModal,
    private routes: ActivatedRoute) { }
  orderId: any;
  auctionId: any;
  orderNumber: any;
  vehicleType: any;
  route: any;
  qty: any;
  orderDate: any;
  startDate: any;
  endDate: any;
  amount: any;
  VendorName: any;
  VendorId: any;
  bidMasterId: any;
  lowestBidAmount: any;
  lastBidAmount: any;
  now:any;
  todaysDataTime:any;
  endDataTime:any;
  isButtonVisible = true;
  vendorUserId:any;
  userId:any;

  keyPress(event: any) {
    // alert(event);
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  isNullorUndefinedorEmpty(str) {
    return (!str || str == '' || str == 'null' || str == '0' || str == null || str == undefined);
  }
  ngOnInit() {

    this.vendorUserId = this.routes.snapshot.params['userId'];

    

    this.orderId = localStorage.getItem("orderId");
    this.orderNumber = localStorage.getItem("orderNumber");
    this.vehicleType = localStorage.getItem("vehicleType");
    this.route = localStorage.getItem("route");
    this.qty = localStorage.getItem("qty");
    this.orderDate = localStorage.getItem("orderDate");
    this.startDate = localStorage.getItem("startDate");
    this.endDate = localStorage.getItem("endDate");
    this.amount = localStorage.getItem("amount");
    this.auctionId = localStorage.getItem("auctionId");
    this.VendorName = localStorage.getItem("vendorName");
    this.VendorId = localStorage.getItem("vendorId");
    this.bidMasterId = localStorage.getItem("bidMasterId");
    this.now = Date.now();
    this.todaysDataTime = formatDate(this.now, 'dd-MM-yyyy hh:mm:ss', 'en-US', '+0530');
    // let momentVariable = moment(this.endDate, 'dd-MM-yyyy hh:mm:ss');  

    // this.endDataTime = formatDate(this.endDate, 'dd-MM-yyyy hh:mm:ss', 'en-US', '+0530');
    
    // alert(this.todaysDataTime+"===="+Date.parse(this.endDate));
    // alert("2nd"+parse(this.endDate, 'dd-MM-yyyy hh:mm:ss', new Date()));

    if(this.todaysDataTime>=this.endDate){
      // alert("exceed");
      this.isButtonVisible = false;
    }

    this.getBidHistory()
  }

  
  getBidHistory(){

    const data ={
      orderId:this.orderId,
      vendorId:this.VendorId,
      auctionId:this.auctionId,
      bidId:this.bidMasterId
    } 

    this.authFackservice.getBidHistory(this.orderId,this.VendorId,this.auctionId,this.bidMasterId).subscribe(data => {
      if (data.status === 200) {
        // this.router.navigate(['/orders/ViewBidList']);
        // this.set.setOption('succesfully Bidded', true);
        
        // this.lowestBidAmount = data.result[0].lowestBidAmount;
        this.lastBidAmount = data.result;

        // if(data.result[0].lastBidAmount === null){
        //   this.lastBidAmount = "-"
        // }else{
        //   this.lastBidAmount = data.result[0].lastBidAmount;
        // }
      }
    }, error => { console.log(error.message); });


  }

  submit(bidamount:any){
    if(bidamount === ""){
      // this.set.setOption('Please Enter Valid Bid amount', false);
      alert("Please Enter Valid Bid amount")
    }else{

      console.log('amount===',bidamount);
      const data ={
        bidAmount : bidamount,
        orderId:this.orderId,
        vendorId:this.VendorId,
        auctionId:this.auctionId,
        bidId:this.bidMasterId
      } 
      this.authFackservice.BiddingAmount(data).subscribe(data => {
        if (data.status === 200) {
          if(this.vendorUserId){
            this.userId = this.vendorUserId 
            this.router.navigate(['/orders/VendorBidList/', this.userId]);
          }else{
            this.router.navigate(['/orders/VendorBidList']);  
          }
          // this.router.navigate(['/orders/VendorBidList']);
          // this.router.navigate(['/orders/VendorBidList/', this.userId]);
          // this.set.setOption('succesfully Bidded', true);
        }else{
          // this.set.setOption('Failed', false);
        }
      }, error => { console.log(error.message); });
    }
    

  }
  

}
function moment(dateString: any, arg1: string) {
  throw new Error('Function not implemented.');
}

