import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VehicleRequirementListComponent} from './vehicle-requirement-list/vehicle-requirement-list.component';
import { VendorMappingComponent } from './vendor-mapping/vendor-mapping.component';
import { ViewAuctionListComponent } from './view-auction-list/view-auction-list.component';
import { ViewBidListComponent } from './view-bid-list/view-bid-list.component';
// import { ViewWonBidListComponent } from './view-won-bid-list/view-won-bid-list.component';
import { PlaceBidComponent } from './place-bid/place-bid.component';
import { EndAuctionComponent } from './end-auction/end-auction.component';
import { VendorBidListComponent } from './vendor-bid-list/vendor-bid-list.component';
import { CreateLhcComponent } from './create-lhc/create-lhc.component';
import { PlaceLhcComponent } from './place-lhc/place-lhc.component';
import { ViewWonBidListComponent } from './view-won-bid-list/view-won-bid-list.component';
import { SelectedAuctionListComponent } from './selected-auction-list/selected-auction-list.component';
let routes:Routes=[
  {
    path: '',
    children: [

  { path: 'VehicleRequirementList', component:VehicleRequirementListComponent,
  data: {
    title: 'Vehicle Requirement List',
    urls: [
      { title: 'Orders', url: '/VehicleRequirementList' },
      { title: 'Vehicle Requirement List' }
    ]
  }
},

{ path: 'VendorMapping', component:VendorMappingComponent,
data: {
  title: 'Vendor Mapping ',
  urls: [
    { title: 'Orders', url: '/VendorMapping' },
    { title: 'Vendor Mapping' }
  ]
}
},

{ path: 'ViewAuctionList', component:ViewAuctionListComponent,
data: {
  title: 'Auction List',
  urls: [
    { title: 'Orders', url: '/ViewAuctionList' },
    { title: 'Auction List' }
  ]
}
},
{ path: 'endAuction', component:EndAuctionComponent,
data: {
  title: 'Auction List',
  urls: [
    { title: 'Orders', url: '/endAuction' },
    { title: 'EndAuctionList' }
  ]
}
},

{ path: 'ViewBidList', component:ViewBidListComponent,
data: {
  title: 'Bid List',
  urls: [
    { title: 'Orders', url: '/ViewBidList' },
    { title: 'Bid List' }
  ]
}
},

{ path: 'VendorBidList/:userId', component:VendorBidListComponent,
data: {
  title: 'Bid List',
  urls: [
    { title: 'Orders', url: '/VendorBidList' },
    { title: 'Bid List' }
  ]
}
},
{ path: 'ViewBidList/auctionId/:auctionId', component:ViewBidListComponent,
data: {
  title: 'Bid List',
  urls: [
    { title: 'Orders', url: '/ViewBidList' },
    { title: 'Bid List' }
  ]
}
},
{ path: 'ViewWonBidList/auctionId/:auctionId', component:ViewWonBidListComponent,
data: {
  title: 'Bid List',
  urls: [
    { title: 'Orders', url: '/ViewBidList' },
    { title: 'Bid List' }
  ]
}
},
{ path: 'PlaceBid', component:PlaceBidComponent,
data: {
  title: 'Place Bid',
  urls: [
    { title: 'Orders', url: '/PlaceBid' },
    { title: 'Place Bid' }
  ]
}
},

{ path: 'PlaceBid/:userId', component:PlaceBidComponent,
data: {
  title: 'Place Bid',
  urls: [
    { title: 'Orders', url: '/PlaceBid' },
    { title: 'Place Bid' }
  ]
}
},


{ path: 'createLhc/:userId', component:CreateLhcComponent,
data: {
  title: 'Create LHC',
  urls: [
    { title: 'Orders', url: '/create LHC' },
    { title: 'create LHC' }
  ]
}
},

{ path: 'placeLhc', component:PlaceLhcComponent,
data: {
  title: 'Place LHC',
  urls: [
    { title: 'Orders', url: '/PlaceLHC' },
    { title: 'Place LHC' }
  ]
}
},
{path:'SelectedAuctionList',component:SelectedAuctionListComponent,
data: {
  title: 'SelectedAuctionList',
  urls: [
    { title: 'Orders', url: '/SelectedAuctionList' },
    { title: 'SelectedAuctionList' }
  ]
}}



]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }
