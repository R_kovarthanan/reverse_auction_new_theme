import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleRequirementListComponent } from './vehicle-requirement-list.component';

describe('VehicleRequirementListComponent', () => {
  let component: VehicleRequirementListComponent;
  let fixture: ComponentFixture<VehicleRequirementListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleRequirementListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleRequirementListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
