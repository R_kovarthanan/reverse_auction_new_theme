import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { NgbTimeStruct, NgbTimeAdapter, NgbTimepickerConfig } from '@ng-bootstrap/ng-bootstrap';

import { AuthfakeauthenticationService } from '../../../core/services/authfake.service';
import { LoaderService } from 'src/app/core/helpers/loader.service';
import { DataService } from 'src/app/data.service';



// export interface orderList {
//   orderNumber: string;
//   vehicleType: string;
// }




@Component({
  selector: 'app-vehicle-requirement-list',
  templateUrl: './vehicle-requirement-list.component.html',
  styleUrls: ['./vehicle-requirement-list.component.css'],
  providers: [NgbTimepickerConfig]
})
export class VehicleRequirementListComponent implements OnInit {

  constructor(private authFackservice: AuthfakeauthenticationService,
    private router: Router, private modalService: NgbModal, config: NgbTimepickerConfig,
    public loaderservice: LoaderService,
    public datal:DataService) {
    config.spinners = false;
  }
  disp1 = "none"
  text = "error"
  userId: any;
  // orderList: orderList[] = [];
  orderList: any;
  searchReportList: any;
  bidStartTime: any;
  bidEndTime: any;
  keyPress(event: any) {
    // alert(event);
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  isNullorUndefinedorEmpty(str) {
    return (!str || str == '' || str == 'null' || str == '0' || str == null || str == undefined);
  }

  map(o: any) {
    localStorage.setItem('orderId', o.orderId);
    localStorage.setItem('orderNumber', o.orderNumber);
    localStorage.setItem('vehicleType', o.vehicleType);
    localStorage.setItem('route', o.route);
    localStorage.setItem('qty', o.totalPackage);
    localStorage.setItem('orderDate', o.orderDate);
    localStorage.setItem('startDate', o.bid_start_date);
    localStorage.setItem('endDate', o.bid_end_date);
    localStorage.setItem('amount', o.bidAmount);
    this.router.navigate(['/orders/VendorMappingComponent']);
  }




  start(o: any) {
    if (this.isNullorUndefinedorEmpty(o.bid_start_date)) {
      // this.set.setOption('Please Enter Valid Bid Start Date', false);
      alert("Please Enter Valid Bid Start Date")
      
      // this.disp1 = "block"
      // this.text = "Please Enter Valid Bid Start Date"
      // setTimeout(() => { this.disp1 = "none"; }, 1500)
    } else if (this.isNullorUndefinedorEmpty(o.bid_end_date)) {
      // this.set.setOption('Please Enter Valid Bid End Date', false);
     alert("Please Enter Valid Bid End Date")

      // this.disp1 = "block"
      // this.text = "Please Enter Valid Bid End Date"
      // setTimeout(() => { this.disp1 = "none"; }, 1500)
    } else if (this.isNullorUndefinedorEmpty(o.bidAmount)) {
      // this.set.setOption('Please Enter Valid Bid Amount', false);
     alert("Please Enter Valid Bid Amount")
    
      // this.disp1 = "block"
      // this.text = "Please Enter Valid Bid Amount"
      // setTimeout(() => { this.disp1 = "none"; }, 1500)
    } else if (this.isNullorUndefinedorEmpty(o.bid_start_time)) {
      // this.set.setOption('Please Enter Valid Bid Start Time', false);
     alert("Please Enter Valid Bid Start Time")
   
      // this.disp1 = "block"
      // this.text = "Please Enter Valid Bid Start Time"
      // setTimeout(() => { this.disp1 = "none"; }, 1500)
    } else if (this.isNullorUndefinedorEmpty(o.bid_end_time)) {
      // this.set.setOption('Please Enter Valid Bid End Time', false);
      alert("Please Enter Valid Bid End Time")
      
      // this.disp1 = "block"
      // this.text = "Please Enter Valid Bid End Time"
      // setTimeout(() => { this.disp1 = "none"; }, 1500)
    }

    else {
      // this.set.setOption(' '+o.orderId,true);

      // console.log("startTime===",o.bid_start_time.hours,":",o.bid_start_time.minute,":",o.bid_start_time.second);
      // console.log("endTimeTime===",o.bid_end_time.hours,":",o.bid_end_time.minutes,":",o.bid_end_time.second);
      // this.bidStartTime = o.bid_start_time.hours,":",o.bid_start_time.minute,":",o.bid_start_time.second;
      // this.bidEndTime = o.bid_end_time.hours,":",o.bid_end_time.minutes,":",o.bid_end_time.second;
      // alert("startTime=="+this.bidStartTime+",bidEndTime=="+this.bidEndTime)
      const data = {
        bidStartDate: o.bid_start_date,
        bidEndDate: o.bid_end_date,
        bidAmount: o.bidAmount,
        bidStartTime: o.bid_start_time,
        bidEndTime: o.bid_end_time
      }
      this.authFackservice.startAuction(o.orderId, data).subscribe(data => {
        if (data.status === 200) {
          this.router.navigate(['/orders/ViewAuctionList']);
        }
      }, error => { console.log(error.message); });
    }
  }
  ngOnInit() {
    this.userId = localStorage.getItem("userId");
    this.getOrderList();
  }
  getOrderList() {
    // this.apiService.getInvoiceListSummary(this.orgId).subscribe(data => {
    this.authFackservice.getOrderList(this.userId).subscribe(data => {
      if (data.status === 200) {
        console.log(data.result);
        this.orderList = data.result;
      }
    }, error => { console.log(error.message); });
  }


  View() {
    localStorage.setItem('userId', this.userId);
    this.router.navigate(['/orders/ViewAuctionListComponent']);
  }


  // time = {hour: 0o0, minute: 0o0};


}
