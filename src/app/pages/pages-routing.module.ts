import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultComponent } from './dashboards/default/default.component';
// import { OrdersModule } from './orders/orders.module';



const routes: Routes = [
{ path: '', redirectTo: 'dashboard' },
{ path: 'dashboard', component: DefaultComponent },
 
  { path: 'contacts', loadChildren: () => import('./contacts/contacts.module').then(m => m.ContactsModule) },
  
 
  { path: 'form', loadChildren: () => import('./form/form.module').then(m => m.FormModule) },

  { path: 'icons', loadChildren: () => import('./icons/icons.module').then(m => m.IconsModule) },

  { path: '', loadChildren: () => import('./orders/orders.module').then(m => m.OrdersModule) },
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
