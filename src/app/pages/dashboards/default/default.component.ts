import { Component, OnInit, ViewChild } from '@angular/core';
import { emailSentBarChart, monthlyEarningChart } from './data';
// import { ChartType } from './dashboard.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';

import { EventService } from '../../../core/services/event.service';

import { ConfigService } from '../../../core/services/config.service';
import { simplePieChart } from 'src/app/chart/apex/data';
import { AuthfakeauthenticationService } from '../../../core/services/authfake.service';
import { ChartType, ChartOptions } from 'node_modules/chart.js';
import * as Highcharts from 'highcharts';
import { Router } from '@angular/router';

@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.scss']
})
export class DefaultComponent implements OnInit {

  isVisible: string;

  emailSentBarChart: ChartType;

  monthlyEarningChart: ChartType;
  highcharts4a: any;
    chartOptions4a: any;
  transactions: Array<[]>;
  statData: Array<[]>;

  isActive: string;

  @ViewChild('content') content;
  constructor(private modalService: NgbModal, private configService: ConfigService, private eventService: EventService,private authFackservice: AuthfakeauthenticationService,
    private router: Router) {
  }
  

  v = 11;
  v1 = 22;
  v2 = 44;
  v3 = 33;
  v4 = 44;
  // series: [66,45,41,17,44]
  set_t = true
  dis = "none"
  simplePieChart: ChartType;
  dashboardData:any;
  total:any;
  
  auction:number;
  salesData:any;
  time: any;
  completed:number;
  ongoingauction:number;
 

  ngOnInit() {

    

    

 this.authFackservice.getDashboardData().subscribe(data => {
            this.salesData = data.result;
            // console.log("salesData==="+JSON.stringify(this.salesData));
            this.highcharts4a = Highcharts;
            this.chartOptions4a = {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                }, credits: {
                    enabled: false
                },
                title: {
                    text: 'Sales Data'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                }, colors: [' #54C571', '#1589FF', '#736F6E', '#FDD017', '#FF5733'],
                series: [{
                    name: 'value',
                    colorByPoint: true,
                    data: this.salesData
                }]
            };

        }, error => console.log(error));
        
 
    // this.simplePieChart = simplePieChart;

   this.getDashboardData();
    
    /**
     * horizontal-vertical layput set
     */
     const attribute = document.body.getAttribute('data-layout');

     this.isVisible = attribute;
     const vertical = document.getElementById('layout-vertical');
     if (vertical != null) {
       vertical.setAttribute('checked', 'true');
     }
     if (attribute == 'horizontal') {
       const horizontal = document.getElementById('layout-horizontal');
       if (horizontal != null) {
         horizontal.setAttribute('checked', 'true');
         console.log(horizontal);
       }
     }

    /**
     * Fetches the data
     */
    // this.fetchData();
  }

  // ngAfterViewInit() {
  //   setTimeout(() => {
  //     // this.openModal();
  //   }, 2000);
  // }

  /**
   * Fetches the data
   */
  private fetchData() {
    this.emailSentBarChart = emailSentBarChart;
    this.monthlyEarningChart = monthlyEarningChart;

    this.isActive = 'year';
    this.configService.getConfig().subscribe(data => {
      this.transactions = data.transactions;
      this.statData = data.statData;
    });
  }

  // openModal() {
  //   this.modalService.open(this.content, { centered: true });
  // }

  // weeklyreport() {
  //   this.isActive = 'week';
  //   this.emailSentBarChart.series =
  //     [{
  //       name: 'Series A',
  //        data: [44, 55, 41, 67, 22, 43, 36, 52, 24, 18, 36, 48]
  //     }, {
  //       name: 'Series B',
  //       data: [11, 17, 15, 15, 21, 14, 11, 18, 17, 12, 20, 18]
  //     }, {
  //       name: 'Series C',
  //       data: [13, 23, 20, 8, 13, 27, 18, 22, 10, 16, 24, 22]
  //     }];
  // }

  // monthlyreport() {
  //   this.isActive = 'month';
  //   this.emailSentBarChart.series =
  //     [{
  //       name: 'Series A',
  //        data: [44, 55, 41, 67, 22, 43, 36, 52, 24, 18, 36, 48]
  //     }, {
  //       name: 'Series B',
  //       data: [13, 23, 20, 8, 13, 27, 18, 22, 10, 16, 24, 22]
  //     }, {
  //       name: 'Series C',
  //       data: [11, 17, 15, 15, 21, 14, 11, 18, 17, 12, 20, 18]
  //     }];
  // }

  // yearlyreport() {
  //   this.isActive = 'year';
  //   this.emailSentBarChart.series =
  //     [{
  //       name: 'Series A',
  //        data: [13, 23, 20, 8, 13, 27, 18, 22, 10, 16, 24, 22]
  //     }, {
  //       name: 'Series B',
  //       data: [11, 17, 15, 15, 21, 14, 11, 18, 17, 12, 20, 18]
  //     }, {
  //       name: 'Series C',
  //       data: [44, 55, 41, 67, 22, 43, 36, 52, 24, 18, 36, 48]
  //     }];
  // }


  /**
   * Change the layout onclick
   * @param layout Change the layout
   */
   changeLayout(layout: string) {
    this.eventService.broadcast('changeLayout', layout);
  }
 
  list :[11, 22, 33, 44]


  getDashboardData(){

    this.authFackservice.getDashboardData().subscribe(data => {
      if (data.status === 200) {
        this.dashboardData = data.result;
        for(var i=0;i<this.dashboardData.length;i++){
        //   this.total = this.dashboardData[i].totalOrder;
        //   this.auction = this.dashboardData[i].totalAuction;
        //   this.completed = this.dashboardData[i].completedAuction;
        //   this.ongoingauction = this.dashboardData[i].ongoingAuction;
          //console.log("total===="+this.total)
          // simplePieChart.series=[this.total,this.auction,this.completed,this.ongoingauction]
       // }   
       this.total = parseInt(this.dashboardData[i].totalOrder)
       this.auction = parseInt(this.dashboardData[i].totalAuction)
       this.completed = parseInt(this.dashboardData[i].completedAuction);
      this.ongoingauction = parseInt(this.dashboardData[i].ongoingAuction);
        }
        
          this.new_func();            
      }
    }, error => { //console.log(error.message);
     });

  }

y = 66

new_func(){
  //console.log(`${vk} oiiiiiiii`)
  //simplePieChart.series.push(122)
  
  var simplePieChart1: ChartType = {
    chart: {
        height: 320,
        type: 'pie',
    },
    // series: [this.total, this.auction, this.completed, this.ongoingauction],
    
    series: [this.total,this.auction,this.completed,this.ongoingauction],
    labels: ['Total Orders', 'Total Auction', 'Completed Auction', 'Ongoing Auction'],
    colors: ['#34c38f', '#556ee6', '#f46a6a', '#50a5f1'],
    legend: {
        show: true,
        position: 'bottom',
        horizontalAlign: 'center',
        verticalAlign: 'middle',
        floating: false,
        fontSize: '14px',
        offsetX: 0,
        offsetY: -10
    },
    responsive: [{
        breakpoint: 600,
        options: {
            chart: {
                height: 240
            },
            legend: {
                show: true
            },
        }
    }]
};





this.simplePieChart = simplePieChart1;
console.log(this.simplePieChart.series);

}

}
