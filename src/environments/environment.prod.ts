export const environment = {
  production: true,
  adminAPIUrl: 'http://124.123.68.18:8086/RAServiceAPI/api/v1/',
  defaultauth: 'fackbackend',
  firebaseConfig: {
    apiKey: '',
    authDomain: '',
    databaseURL: '',
    projectId: '',
    storageBucket: '',
    messagingSenderId: '',
    appId: '',
    measurementId: ''
  }
};
